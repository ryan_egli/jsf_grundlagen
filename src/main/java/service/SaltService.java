package service;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.persistence.Query;

import model.Salt;

import org.apache.log4j.Logger;

@SessionScoped
public class SaltService extends AbstractService<Salt> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(SaltService.class);

	public SaltService() {
		super(Salt.class);
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void init() {
		//removeSalt();
		//createSalt();
	}

	public void removeSalt() {
		Query q = em.createQuery("FROM Salt", Salt.class);
		List<Salt> saltList = q.getResultList();

		try {
			em.getTransaction().begin();
			for (Salt s : saltList) {
				System.out.println("SaltService.removeSalt() " + s);
				em.remove(s);
			}
			em.getTransaction().commit();

		} catch (Exception e) {

			if (em.getTransaction() != null && em.getTransaction().isActive())
				em.getTransaction().rollback();

			throw e;
		}

	}

	public void createSalt() {
		for (int i = 0; i < 10; i++) {
			Salt salt = new Salt();

			byte[] array = new byte[10];
			new Random().nextBytes(array);
			String generatedString = new String(array, Charset.forName("UTF-8"));
			System.out.println("Random Salt :  " + generatedString);
			LOGGER.info("Random Salt :  " + generatedString);
			salt.setSalt(generatedString);

			try {
				em.getTransaction().begin();

				em.merge(salt);

				em.getTransaction().commit();

			} catch (Exception e) {

				if (em.getTransaction() != null
						&& em.getTransaction().isActive())
					em.getTransaction().rollback();

				throw e;
			}

		}
	}

}
