package service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Kanton;
import model.Kunde;

public class AuswertungService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager em;
	
	public AuswertungService() {
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Kanton> getKantoneFromKunden() {
		Query q = em.createQuery("SELECT DISTINCT k.kanton FROM Kunde k where k.kanton != null", Kanton.class);
		
		List<Kanton> kantonList = q.getResultList();
		
		return kantonList;


	}
	
	@SuppressWarnings("unchecked")
	public List<Kunde> getKundenProKanton(Kanton kanton) {
		Query q = em.createQuery("FROM Kunde where kanton.id = :idKanton", Kunde.class).setParameter("idKanton", kanton.getId());
		
		List<Kunde> kundeList = q.getResultList();
		
		return kundeList;


	}
	
	
}
