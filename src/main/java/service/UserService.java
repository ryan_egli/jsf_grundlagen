package service;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import controller.SaltController;
import model.Salt;
import model.User;

public class UserService extends AbstractService<User> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	@Inject
	private SaltController saltController;

	@Inject
	private SaltService saltService;

	public UserService() {
		super(User.class); // TODO Auto-generated constructor
	}

	@Override
	protected void customSave(User objectToSave) throws Exception {
		super.customSave(objectToSave);

		if (objectToSave.getPassword() != null) {
			objectToSave = setPassword(objectToSave);
		}
	}

	/*
	 * @Override public User save(User objectToSave) {
	 * System.out.println("UserService.save()"); try { em.getTransaction().begin();
	 * 
	 * 
	 * 
	 * 
	 * objectToSave = em.merge(objectToSave);
	 * 
	 * em.getTransaction().commit();
	 * 
	 * return objectToSave; } catch (Exception e) {
	 * 
	 * if (em.getTransaction() != null && em.getTransaction().isActive())
	 * em.getTransaction().rollback();
	 * 
	 * System.out.println("UserService.save()" + e); } return null; }
	 */

	public User checkIfUsernameExists(User u) {

		List<User> userList = fetchRecords();
		for (User userListUser : userList) {
			if (userListUser.getUsername().equalsIgnoreCase(u.getUsername())) {
				return userListUser;
			}
		}

		return null;

	}

	public User setPassword(User userToSave) throws NoSuchAlgorithmException, NoSuchProviderException {

		String passwordToHash = userToSave.getPassword();
		userToSave.setSalt(getSalt());

		String regeneratedPassowrdToVerify = getSecurePassword(passwordToHash,
				userToSave.getSalt().getSalt().getBytes());

		userToSave.setPasswordHashed(regeneratedPassowrdToVerify);

		return userToSave;

	}

	public String setPassword(String passwordForPasswordHash, Salt existingSalt)
			throws NoSuchAlgorithmException, NoSuchProviderException {

		String passwordToHash = passwordForPasswordHash;
		byte[] salt = existingSalt.getSalt().getBytes();

		String regeneratedPassowrdToVerify = getSecurePassword(passwordToHash, salt);
		return regeneratedPassowrdToVerify;

	}

	private static String getSecurePassword(String passwordToHash, byte[] salt) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");

			md.update(salt);

			byte[] bytes = md.digest(passwordToHash.getBytes());

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println("UserService.getSecurePassword() hashedPassword: " + generatedPassword);
		return generatedPassword;
	}

	// Add salt
	private Salt getSalt() throws NoSuchAlgorithmException, NoSuchProviderException {

		System.out.println("UserService.getSalt()" + saltService.fetchRecords());
		saltService.fetchRecords();
		List<Salt> saltList = saltService.fetchRecords();

		Random rand = new Random();
		Salt randomSalt = saltList.get(rand.nextInt(saltList.size()));

		saltController.setSelectedRecord(randomSalt);

		// return salt
		return randomSalt;
	}

	public User validate(String username, String passwordClearText) {
		Query q = em.createQuery("From User where username = :username", User.class);
		q.setParameter("username", username);

		List<User> userList = q.getResultList();

		for (User u : userList) {
			System.out.println("UserService.validate()" + u.getSalt().getSalt());
		}

		String passwordHashed = "";
		if (username != null && passwordClearText != null) {
			System.out.println("UserService.validate() username & passwort �bergeben");

			for (User u : userList) {

				try {
					passwordHashed = setPassword(passwordClearText, u.getSalt());
				} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
					e.printStackTrace();
				}

				if (u.getPasswordHashed().equals(passwordHashed)) {
					System.out.println("UserService.validate() " + u.getPassword());
					System.out.println("UserService.validate() " + u.getPasswordHashed());
					System.out.println("UserService.validate() " + passwordHashed);
					return u;
				} else {
					System.out.println("UserService.validate() " + u.getPassword());
					System.out.println("UserService.validate() " + u.getPasswordHashed());
					System.out.println("UserService.validate() " + passwordHashed);
				}
			}
		}

		return null;
	}

}
