package service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Mutable;
import controller.UserController;

public abstract class AbstractService<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// GRRRRR
	@Inject
	private UserController userController;
	
	private Class<T> clazz;

	@Inject
	protected EntityManager em;
	
	public AbstractService() {
		super();
	}

	public AbstractService(Class<T> clazz) {
		super();
		this.clazz = clazz;
	}

	public List<T> fetchRecords() {
		String qlString = "FROM " + clazz.getSimpleName();
		System.out.println(qlString);
		Query q = em.createQuery(qlString);
		// q.setParameter("T", clazz.getSimpleName());
		System.out.println(em.createQuery(qlString));
		List<T> kList = (List<T>) q.getResultList();

		if (!kList.isEmpty()) {
			return kList;
		}

		return null;
	}

	protected void customSave(T objectToSave) throws Exception {
		if (objectToSave instanceof Mutable) {
			((Mutable) objectToSave).setMutiertvon(userController.getSelectedRecord());
		}		
	}
	
	public T save(T objectToSave) throws Exception {		
		try {
			em.getTransaction().begin();
			
			customSave(objectToSave);
			
			objectToSave = em.merge(objectToSave);

			em.getTransaction().commit();

			return objectToSave;
		} catch (Exception e) {

			if (em.getTransaction() != null && em.getTransaction().isActive())
				em.getTransaction().rollback();

			throw e;
		}
	}

	public void remove(T objectToRemove) {
		try {
			em.getTransaction().begin();
			if(em.contains(objectToRemove)){
				em.remove(objectToRemove);
			}else{
				objectToRemove = em.merge(objectToRemove);
				em.remove(objectToRemove);
			}
			em.getTransaction().commit();

		} catch (Exception e) {

			if (em.getTransaction() != null && em.getTransaction().isActive())
				em.getTransaction().rollback();

			throw e;
		}
	}
}
