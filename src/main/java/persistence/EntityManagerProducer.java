package persistence;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Named("entityManagerProducer")
@ApplicationScoped
public class EntityManagerProducer {

	private static EntityManagerFactory emfProd;
	
	private static EntityManagerFactory emfTests;
	
	
	@PostConstruct
    public void init(){
        createEntityManagerFactoryProd();
        createEntityManagerFactoryTest();
	}
	
    private EntityManagerFactory createEntityManagerFactoryProd() {
        if (emfProd == null || !emfProd.isOpen()) {
        	emfProd = Persistence.createEntityManagerFactory("pu");
        }
        return emfProd;
    }
    
    private EntityManagerFactory createEntityManagerFactoryTest() {
        if (emfTests == null || !emfTests.isOpen()) {
        	emfTests = Persistence.createEntityManagerFactory("pu_test");
        }
        return emfTests;
    }

	
	@Produces
	@RequestScoped
	@Default
	public EntityManager createEntityManagerProd() {
        return createEntityManagerFactoryProd().createEntityManager();
	}
	
	@Produces
	@RequestScoped
	@EmfTest
	public EntityManager createEntityManagerTest() {
		return createEntityManagerFactoryTest().createEntityManager();
	}
	
	
}
