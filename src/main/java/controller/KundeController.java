package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.Kunde;
import service.KundeService;

@Named("kundeController")
@SessionScoped
public class KundeController extends AbstractController<Kunde> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public KundeController() {

	}

	@Inject
	public KundeController(KundeService kundeService) {
		super(kundeService, Kunde.class);
	}

	public void saveKunde() {

		if (getSelectedRecord() != null) {
			save();

			showInfo("Speichern erfolgreich", getSelectedRecord().getPerson()
					.getName());
		} else {
			showError("Speichern fehlgeschlagen", "");
		}
	}

	public void removeKunde() {
		remove();
		showInfo("L�schen erfolgreich", "");
	}
}
