package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import service.AbstractService;

public abstract class AbstractController<T> implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private transient T selectedRecord = null;
	
	private transient List<T> recordList = new ArrayList<T>();
	private transient List<T> filteredRecordList = new ArrayList<T>();
	private Class<T> clazz;
	private transient AbstractService<T> dataService;
	

	public AbstractController() {

	}

	public AbstractController(AbstractService<T> service, Class<T> clazz) {
		this.dataService = service;
		this.clazz = clazz;
	}
	
	@PostConstruct
	public void init() {
		fetch();
	}
	

	public void fetch(){
		recordList = dataService.fetchRecords();
		setFilteredRecordList(getRecordList());
	}
	
	public void create() {
		try {
			selectedRecord = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void save() {
		try {
			dataService.save(selectedRecord);
		} catch (Exception e) {
			e.printStackTrace();
		}
		fetch();
	}
	
	public void remove(){
		dataService.remove(selectedRecord);
		fetch();
	}
	
	public void showInfo(String title, String zusatz){
		message(title, zusatz, FacesMessage.SEVERITY_INFO);
	}
	
	public void showError(String title, String zusatz){
		message(title, zusatz, FacesMessage.SEVERITY_ERROR);
	} 
	
	public void message(String title, String zusatz, Severity facesMessage){
		FacesMessage message = new FacesMessage(facesMessage,
				title, zusatz);
		FacesContext.getCurrentInstance().addMessage("", message);
	}
	
	public T getSelectedRecord(){
		return selectedRecord;
	}
	
	
	public void setSelectedRecord(T selectedRecord) {
		this.selectedRecord = selectedRecord;
	}


	public List<T> getRecordList() {
		return recordList;
	}

	public List<T> getFilteredRecordList() {
		return filteredRecordList;
	}

	public void setFilteredRecordList(List<T> filteredRecordList) {
		this.filteredRecordList = filteredRecordList;
	}
	
	public AbstractService<T> getDataService() {
		return dataService;
	}

	
	
}
