package controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import model.Kanton;
import model.Kunde;
import service.AuswertungService;

@Named("auswertungController")
@SessionScoped
public class AuswertungController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7601042262972196162L;

	private PieChartModel kundeProKantonPieModel;
	private BarChartModel kundeProKantonBarModel;

	@Inject
	private AuswertungService auswertungService;

	@PostConstruct
	public void createModels() {
		createPieModels();
		createBarModels();
	}

	private void createBarModels() {
		createKundeProDatumModel();
	}

	private void createPieModels() {
		createKundeProKantonModel();
	}

//	public static List<?> removeDuplicate(List<?> list) {
//		Set<?> set = listToSet(list);
//		List<?> listWithoutDuplicate = setToList(set);
//		return listWithoutDuplicate;
//	}
//
//	public static Set<?> listToSet(List<?> list) {
//		Set<Object> set = new LinkedHashSet<>();
//		set.addAll(list);
//		return set;
//	}
//
//	public static List<?> setToList(Set<?> set) {
//		List<Object> list = new ArrayList<>();
//		list.addAll(set);
//		return list;
//	}

	
	
	
	
	
	
//	public void createJasperReport() {
//		try {
//
//			ClassLoader classLoader = getClass().getClassLoader();
//			
//			System.out.println(classLoader.getResourceAsStream("reports/SampleFunctionsReport.jrxml").toString());
//			
//			classLoader.getResourceAsStream("reports/SampleFunctionsReport.jrxml");
//			
//			JasperReport jasperReport = JasperCompileManager
//					.compileReport(classLoader.getResourceAsStream("reports/SampleFunctionsReport.jrxml"));
//
//			Map<String, Object> parameters = new HashMap<String, Object>();
//			
//			JRDataSource dataSource = new JREmptyDataSource();			
//			
//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
//
//			File outDir = new File("C:/Users/Egli/Desktop");
//			outDir.mkdirs();
//
//			// Export to PDF.
//			JasperExportManager.exportReportToPdfFile(jasperPrint, "C:/Users/Egli/Desktop/test.pdf");
//
//			System.out.println("Done!");
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void createKundeProDatumModel() {
		kundeProKantonBarModel = initBarModel();

		kundeProKantonBarModel.setTitle("Kunden pro Kanton");
		kundeProKantonBarModel.setLegendPosition("ne");

		Axis xAxis = kundeProKantonBarModel.getAxis(AxisType.X);
		// xAxis.setLabel("Kantone");

		Axis yAxis = kundeProKantonBarModel.getAxis(AxisType.Y);
		yAxis.setLabel("Anzahl Kunden");

		int maxKundenProKanton = 0;
		List<Kanton> kantonList = auswertungService.getKantoneFromKunden();
		for (Kanton k : kantonList) {
			List<Kunde> kundeList = auswertungService.getKundenProKanton(k);
			if (kundeList.size() > maxKundenProKanton) {
				maxKundenProKanton = kundeList.size();
			}
		}
		maxKundenProKanton += 2;
		yAxis.setMin(0);
		yAxis.setMax(maxKundenProKanton);
		yAxis.setTickInterval("1");
	}

	private BarChartModel initBarModel() {
		BarChartModel model = new BarChartModel();

		List<Kanton> kantonList = auswertungService.getKantoneFromKunden();

		System.out
				.println("AuswertungController.initBarModel() kantonListWithoutDuplicate.size(): " + kantonList.size());

		ChartSeries kantone = new ChartSeries();

		for (Kanton k : kantonList) {
			List<Kunde> kundeList = auswertungService.getKundenProKanton(k);

			kantone.setLabel(k.getText());
			kantone.set("Aufteilung Kantone", kundeList.size());

			System.out.println("AuswertungController.initBarModel() k.getText(), kundeList.size(): " + k.getText()
					+ "  " + kundeList.size());

			model.addSeries(kantone);

			kantone = new ChartSeries();
		}

		return model;
	}

	private void createKundeProKantonModel() {
		kundeProKantonPieModel = new PieChartModel();

		List<Kanton> kantonList = auswertungService.getKantoneFromKunden();

		// List<Kanton> kantonListWithoutDuplicate = (List<Kanton>)
		// removeDuplicate(kantonList);
		for (Kanton k : kantonList) {
			List<Kunde> kundeList = auswertungService.getKundenProKanton(k);

			kundeProKantonPieModel.getData().put(k.getText(), kundeList.size());

		}
		kundeProKantonPieModel.setTitle("Kunden pro Kanton");
		kundeProKantonPieModel.setLegendPosition("ne");

	}

	public PieChartModel getKundeProKantonPieModel() {
		return kundeProKantonPieModel;
	}

	public void setKundeProKantonPieModel(PieChartModel kundeProKantonModel) {
		this.kundeProKantonPieModel = kundeProKantonModel;
	}

	public BarChartModel getKundeProKantonBarModel() {
		return kundeProKantonBarModel;
	}

	public void setKundeProKantonBarModel(BarChartModel kundeProDatumModel) {
		this.kundeProKantonBarModel = kundeProDatumModel;
	}

}
