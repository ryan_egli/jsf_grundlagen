package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.User;
import service.UserService;

@Named("userController")
@SessionScoped
public class UserController extends AbstractController<User> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	
	public UserController() {

	}
	@Inject
	private UserService userService;

	@Inject
	public UserController(UserService userService) {
		super(userService, User.class);
	}

	public void saveUser(){
		if (getSelectedRecord() != null) {
			
			if(userService.checkIfUsernameExists(getSelectedRecord()) == null){//
				
				try {
					userService.save(getSelectedRecord());
				} catch (Exception e) {
					e.printStackTrace();
				}

				showInfo("Speichern erfolgreich", getSelectedRecord().getPerson()
						.getName());
			}else{
				showError("Username bereits vorhanden", "Speichern fehlgeschlagen");
			}
			
		} else {
			showError("Speichern fehlgeschlagen", "");
		}
		

	}

	public void removeUser() {
		remove();
		showInfo("L�schen erfolgreich", "");
	}
	
	
}
