package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.Salt;
import service.SaltService;

@Named("saltController")
@SessionScoped
public class SaltController extends AbstractController<Salt> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public SaltController() {

	}

	@Inject
	public SaltController(SaltService saltService) {
		super(saltService, Salt.class);
	}
	
	
	@Inject
	private SaltService saltService;

	public void saveSalt() {
		
		saltService.getClass();

		if (getSelectedRecord() != null
				&& getSelectedRecord().getSalt() != null) {
			save();

			showInfo("Speichern erfolgreich", getSelectedRecord().getSalt());

		} else {
			showError("Speichern fehlgeschlagen", "");
		}

	}

	public void removeSalt() {
		remove();
		showInfo("L�schen erfolgreich", "");
	}
}
