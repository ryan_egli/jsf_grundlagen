package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.User;
import service.UserService;

@Named("currentUserController")
@SessionScoped
public class CurrentUserController extends AbstractController<User> implements
		Serializable {

	private static final long serialVersionUID = 1L;
	
	public CurrentUserController() {

	}

	@Inject
	public CurrentUserController(UserService userService) {
		super(userService, User.class);
	}

	
	
}
