package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.Kanton;
import service.KantonService;

@Named("kantonController")
@SessionScoped
public class KantonController extends AbstractController<Kanton> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public KantonController() {

	}

	@Inject
	public KantonController(KantonService kantonService) {
		super(kantonService, Kanton.class);
	}

	public void saveKanton() {

		if (getSelectedRecord() != null
				&& getSelectedRecord().getText() != null) {
			save();

			showInfo("Speichern erfolgreich", getSelectedRecord().getText());

		} else {
			showError("Speichern fehlgeschlagen", "");
		}

	}

	public void removeKanton() {
		remove();
		showInfo("L�schen erfolgreich", "");
	}
}
