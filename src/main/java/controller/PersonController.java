package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.Person;
import service.PersonService;

@Named("personController")
@SessionScoped
public class PersonController extends AbstractController<Person> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public PersonController() {

	}

	@Inject
	public PersonController(PersonService personService) {
		super(personService, Person.class);
	}

	public void savePerson() {

		if (getSelectedRecord() != null
				&& getSelectedRecord().getName() != null) {
			save();

			showInfo("Speichern erfolgreich", getSelectedRecord().getName());

		} else {
			showError("Speichern fehlgeschlagen", "");
		}

	}

	public void removePerson() {
		remove();
		showInfo("L�schen erfolgreich", "");
	}
	
	public void check() {
		System.out.println(getSelectedRecord());
		if(getSelectedRecord() != null){
			System.out.println(getSelectedRecord().getName());
			System.out.println(getSelectedRecord().getEmail());
			System.out.println(getSelectedRecord().getVorname());
		}
	}
}
