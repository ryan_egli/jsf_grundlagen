package controller;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;

import model.Mutable;
import model.User;

@Embeddable
public class EmbeddedMutable implements Serializable, Mutable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date mutiertam;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    private User mutiertvon;

    @Override
    public Date getMutiertam() {
        return mutiertam;
    }
    
    @PreUpdate
    public void preUpdate(){
    	this.mutiertam = new Date();
    }

    @Override
    public void setMutiertam(Date mutiertam) {
        this.mutiertam = mutiertam;
    }

    @Override
    public User getMutiertvon() {
        return mutiertvon;
    }

    @Override
    public void setMutiertvon(User mutiertvon) {
        this.mutiertvon = mutiertvon;
    }
    
}
