package filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.weld.security.GetSystemPropertyAction;

import model.User;
import service.UserService;
import controller.CurrentUserController;
import controller.UserController;

/**
 * Servlet Filter implementation class AuthenticationFilter
 */
@WebFilter("/pages/*")
public class AuthenticationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthenticationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Inject
	private UserService userService;
	
	@Inject
	private CurrentUserController currentUserController;
	
	
	@Inject
	private UserController userController;
	
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		String username = request.getParameter("username");
		
		String passwordClearText = request.getParameter("password");
		User user = null;
		
		if(username != null && passwordClearText != null){
			user = userService.validate(username, passwordClearText);
		}
		
		//Object valueTest = (HttpServletRequest)request).getSession().getAttribute("isLoggedIn");
		
		Object value = ((HttpServletRequest)request).getSession().getAttribute("isLoggedIn");
		
		if(user != null){
			System.out.println("LoginServlet.doGet() User is logged in");
			userController.setSelectedRecord(user);
			
			((HttpServletRequest)request).getSession().setAttribute("isLoggedIn", "true");
			((HttpServletResponse)response).sendRedirect("/pages/index.jsf");
			currentUserController.setSelectedRecord(user);
			
			chain.doFilter(request, response);
		}else if(value != null){
			((HttpServletRequest)request).getSession().setAttribute("isLoggedIn", "true");
			chain.doFilter(request, response);
		}else{
			System.out.println("LoginServlet.doGet() User is not logged in");
			((HttpServletResponse) response).sendRedirect("/login.html");
		}
		
	}
	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
