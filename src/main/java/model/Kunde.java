package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import controller.EmbeddedMutable;

@Entity
@Table(name = "KUNDE")
public class Kunde implements Serializable, Keyable, Mutable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID_KUNDE")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ID_PERSON")
	private Person person;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "ID_KANTON")
	private Kanton kanton;

	@Embedded
	private EmbeddedMutable mutable = new EmbeddedMutable();

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Kanton getKanton() {
		return kanton;
	}

	public void setKanton(Kanton kanton) {
		this.kanton = kanton;
	}

	public EmbeddedMutable getMutable() {
		return mutable;
	}

	public void setMutable(EmbeddedMutable mutable) {
		this.mutable = mutable;
	}
	
	@Override
    public Date getMutiertam() {
        return mutable.getMutiertam();
    }

    @Override
    public User getMutiertvon() {
        return mutable.getMutiertvon();
    }

    @Override
    public void setMutiertam(Date mutiertam) {
        mutable.setMutiertam(mutiertam);
    }

    @Override
    public void setMutiertvon(User mutiertvon) {
        mutable.setMutiertvon(mutiertvon);
    }

}