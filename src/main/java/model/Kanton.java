package model;

import java.io.Serializable;
import java.util.Date;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import controller.EmbeddedMutable;


@Entity
@Table(name = "KANTON")
public class Kanton implements Serializable,  Mutable, Keyable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String text;
	
	private String kurzbezeichnung;
	
	@Embedded
	private EmbeddedMutable mutable = new EmbeddedMutable();
	
	
	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer i) {
		this.id = i;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getKurzbezeichnung() {
		return kurzbezeichnung;
	}
	public void setKurzbezeichnung(String kurzbezeichnung) {
		this.kurzbezeichnung = kurzbezeichnung;
	}
	
	@Override
    public Date getMutiertam() {
        return mutable.getMutiertam();
    }

    @Override
    public User getMutiertvon() {
        return mutable.getMutiertvon();
    }

    @Override
    public void setMutiertam(Date mutiertam) {
        mutable.setMutiertam(mutiertam);
    }

    @Override
    public void setMutiertvon(User mutiertvon) {
        mutable.setMutiertvon(mutiertvon);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Kanton))
			return false;
		Kanton other = (Kanton) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}

	
    
    
	
}