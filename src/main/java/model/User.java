package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import controller.EmbeddedMutable;

@Entity
@Table(name = "`USER`")
public class User implements Serializable, Keyable, Mutable {
	

	private static final long serialVersionUID = 6452366309968252633L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "PERSON")
	private Person person;
	
	private transient String password;
	
	private String passwordHashed;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "SALT")
	private Salt salt;
	
	private String username;
	
	@Embedded
	private EmbeddedMutable mutable = new EmbeddedMutable();
	

	@Override
	public Integer getId() {
		return id;
	}


	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Person getPerson() {
		return person;
	}



	public void setPerson(Person person) {
		this.person = person;
	}



	public Salt getSalt() {
		return salt;
	}



	public void setSalt(Salt salt) {
		this.salt = salt;
	}



	public EmbeddedMutable getMutable() {
		return mutable;
	}



	public void setMutable(EmbeddedMutable mutable) {
		this.mutable = mutable;
	}

	
	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getPasswordHashed() {
		return passwordHashed;
	}



	public void setPasswordHashed(String passwordHashed) {
		this.passwordHashed = passwordHashed;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	@Override
    public Date getMutiertam() {
        return mutable.getMutiertam();
    }

    @Override
    public User getMutiertvon() {
        return mutable.getMutiertvon();
    }

    @Override
    public void setMutiertam(Date mutiertam) {
        mutable.setMutiertam(mutiertam);
    }

    @Override
    public void setMutiertvon(User mutiertvon) {
        mutable.setMutiertvon(mutiertvon);
    }

	
}