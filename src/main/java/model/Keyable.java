package model;

public interface Keyable {

    Integer getId();

    void setId(Integer id);
}
