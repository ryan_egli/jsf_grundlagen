package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import controller.EmbeddedMutable;


@Entity
@Table(name = "PERSON")
public class Person implements Serializable, Keyable, Mutable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String name;
	
	private String vorname;
	
	private String email;
	
	private String telefon;
	
	private Boolean juristisch;
	
	@Embedded
	private EmbeddedMutable mutable = new EmbeddedMutable();

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public Boolean getJuristisch() {
		return juristisch;
	}

	public void setJuristisch(Boolean juristisch) {
		this.juristisch = juristisch;
	}

	public EmbeddedMutable getMutable() {
		return mutable;
	}

	public void setMutable(EmbeddedMutable mutable) {
		this.mutable = mutable;
	}
	
	public String getFullName(){
		return name + " " + vorname;
	}

	@Override
    public Date getMutiertam() {
        return mutable.getMutiertam();
    }

    @Override
    public User getMutiertvon() {
        return mutable.getMutiertvon();
    }

    @Override
    public void setMutiertam(Date mutiertam) {
        mutable.setMutiertam(mutiertam);
    }

    @Override
    public void setMutiertvon(User mutiertvon) {
        mutable.setMutiertvon(mutiertvon);
    }
	
}