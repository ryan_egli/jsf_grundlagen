package model;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import controller.EmbeddedMutable;

@Entity
@Table(name = "SALT")
public class Salt implements Serializable, Keyable, Mutable{
	

	private static final long serialVersionUID = 6452366309968252633L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String salt;
	
	@Embedded
	private EmbeddedMutable mutable = new EmbeddedMutable();

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String hash) {
		this.salt = hash;
	}

	public EmbeddedMutable getMutable() {
		return mutable;
	}

	public void setMutable(EmbeddedMutable mutable) {
		this.mutable = mutable;
	}
	
	@Override
    public Date getMutiertam() {
        return mutable.getMutiertam();
    }

    @Override
    public User getMutiertvon() {
        return mutable.getMutiertvon();
    }

    @Override
    public void setMutiertam(Date mutiertam) {
        mutable.setMutiertam(mutiertam);
    }

    @Override
    public void setMutiertvon(User mutiertvon) {
        mutable.setMutiertvon(mutiertvon);
    }
	
	
}