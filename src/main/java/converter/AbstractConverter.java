package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import model.Keyable;

/**
 * Generische Superklasse für alle Converter. Die Subklassen müssen als
 * request-scoped Beans annotiert werden.
 * 
 * Ist JSF 2.1 kompatibler Converter. Kann ab JSF 2.3 umgebaut werden.
 * http://stackoverflow
 * .com/questions/7665673/jsf2-can-i-use-ejb-to-inject-a-service
 * -into-a-facesconverter
 * 
 * @see
 * @author cerny
 * 
 */

public abstract class AbstractConverter implements Converter {

	@Inject
	protected EntityManager em;

	private Class clazz;

	public AbstractConverter() {
		super();
	}

	public AbstractConverter(Class clazz) {
		super();
		this.clazz = clazz;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return (value == null) ? null : em
				.find(clazz, Integer.valueOf(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return (value == null) ? "" : String.valueOf(((Keyable) value).getId());
	}
}
