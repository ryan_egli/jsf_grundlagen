package converter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import model.User;

@Named("userConverter")
@RequestScoped
public class UserConverter extends AbstractConverter {

	public UserConverter() {
		super(User.class);
	}
}