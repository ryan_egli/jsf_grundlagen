package converter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import model.Kanton;

@Named("kantonConverter")
@RequestScoped
public class KantonConverter extends AbstractConverter {

	public KantonConverter() {
		super(Kanton.class);
	}
}