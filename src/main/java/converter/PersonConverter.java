package converter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import model.Person;

@Named("personConverter")
@RequestScoped
public class PersonConverter extends AbstractConverter {

	public PersonConverter() {
		super(Person.class);
	}
}