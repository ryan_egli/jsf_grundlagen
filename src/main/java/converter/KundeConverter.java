package converter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import model.Kunde;

@Named("kundeConverter")
@RequestScoped
public class KundeConverter extends AbstractConverter {

	public KundeConverter() {
		super(Kunde.class);
	}
}