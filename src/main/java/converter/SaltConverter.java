package converter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import model.Salt;

@Named("saltConverter")
@RequestScoped
public class SaltConverter extends AbstractConverter {

	public SaltConverter() {
		super(Salt.class);
	}
}