package controller;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import controller.UserController;
import model.User;

@Named("userReflection")
@SessionScoped
public class UserReflection extends AbstractReflection<User> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	
	public UserReflection() {
		super(User.class);
		// TODO Auto-generated constructor stub
	}
	
}
