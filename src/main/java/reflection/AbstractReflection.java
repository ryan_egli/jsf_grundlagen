package controller;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import controller.EmbeddedMutable;
import controller.UserController;
import service.AbstractService;

public abstract class AbstractReflection<T> implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Class<T> clazz;
	
	private List<Field> fields;
	
	
	private List<String> excludetFields = new ArrayList<String>();
	

	public AbstractReflection() {

	}
	
	public AbstractReflection(Class<T> clazz) {
		super();
		this.clazz = clazz;
	}

	
	public List<Field> getFields(){
		return getFields(true, true, true);
	}
	
	public List<Field> getFields(Boolean printMutable, Boolean printActivable, Boolean printSortable){

        Field[] allFields = clazz.getDeclaredFields();

        for (Field f : allFields) {
            System.out.println(f.getName());
        }

        List<Field> fieldListMethod = new ArrayList<Field>(Arrays.asList(allFields));

        for (Field f : fieldListMethod) {
            System.out.println(f.getName());
        }

        List<Field> fieldListEditable = new ArrayList<Field>();
        fieldListEditable.addAll(fieldListMethod);

        if(fieldListMethod != null && !fieldListMethod.isEmpty()){
            for(Field f : fieldListMethod){
            	System.out.println(f.getName() + "    " + f.getType().toGenericString() + "     " + f.getType().toString());
                if(!printMutable){
                    if(f.getType().isAssignableFrom(EmbeddedMutable.class)) {
                        fieldListEditable.remove(f);
                    }
                }else{
                   // System.out.println(f.getName() + "    " + f.getType().toGenericString() + "     " + f.getType().toString());
                    if(f.getType().isAssignableFrom(EmbeddedMutable.class)) {
                        fieldListEditable.addAll(Arrays.asList(f.getType().getDeclaredFields()));
                        fieldListEditable.remove(f);

                    }
                }

                if(!printActivable){

                }

                if(!printSortable){

                }

                if(excludetFields.contains(f.getName())){
                   fieldListEditable.remove(f);
                }
            }
            this.fields = fieldListEditable;
            return fieldListEditable;
        }else{
            return null;
        }

    }
	

	public Class<T> getClazz() {
		return clazz;
	}


	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public List<String> getExcludetFields() {
		return excludetFields;
	}

	public void setExcludetFields(String... excludetFields) {
		for(String field : excludetFields){
			this.excludetFields.add(field);
		}
	}

}
