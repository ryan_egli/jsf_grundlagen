package password.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import controller.PersonController;
import controller.UserController;
import model.Person;
import model.User;

@RunWith(WeldJUnit4Runner.class)
public class PasswordTest {

	@Inject
	private PersonController personController;
	
	@Inject
	private UserController userController;
	
    @Test
    public void testPasswordSave() {
    	
    	Person person = new Person();
    	person.setEmail("test@exemple.ch");
    	person.setName("Name");
    	person.setVorname("Vorname");
    	person.setJuristisch(false);
    	person.setTelefon("+41 79 557 80 36");
    	
    	personController.setSelectedRecord(person);
    	personController.savePerson();
    	
    	
    	User user = new User();
    	user.setPerson(personController.getSelectedRecord());
    	user.setPassword("MyUltraSecurePassword");
    	user.setUsername("username");
    	userController.setSelectedRecord(user);
    	
    	userController.saveUser();
    	
    	
    	List<User> userList = userController.getRecordList();
    	
    	assertEquals(1, userList.size());
    	
    	
    }

}