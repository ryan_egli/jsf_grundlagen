package org.jboss.weld.manager;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;

import org.jboss.weld.bean.builtin.BeanManagerProxy;
import org.jboss.weld.context.AbstractBoundContext;
import org.jboss.weld.context.bound.MutableBoundRequest;
import org.jboss.weld.manager.BeanManagerImpl;

/**
 * Taken from http://www.jtips.info/index.php?title=WeldSE/Scopes, it simulates
 * request and session scopes outside of an application server.
 */
public class WeldServletScopesSupportForSe implements Extension {

	/** {@inheritDoc} */
	public void afterDeployment(@Observes final AfterDeploymentValidation event, final BeanManager beanManager) {

		Map<String, Object> applicationMap = new HashMap<String, Object>();
		activateContext(beanManager, ApplicationScoped.class, applicationMap);

		Map<String, Object> sessionMap = new HashMap<String, Object>();
		activateContext(beanManager, SessionScoped.class, sessionMap);

		Map<String, Object> requestMap = new HashMap<String, Object>();
		activateContext(beanManager, RequestScoped.class, requestMap);

		activateContext(beanManager, ConversationScoped.class, new MutableBoundRequest(requestMap, sessionMap));
	}

	/**
	 * Activates a context for a given manager.
	 * 
	 * @param beanManager in which the context is activated
	 * @param cls         the class that represents the scope
	 * @param storage     in which to put the scoped values
	 * @param             <S> the type of the storage
	 */
	private <S> void activateContext(final BeanManager beanManager, final Class<? extends Annotation> cls,
			final S storage) {
		BeanManagerImpl beanManagerImpl = (BeanManagerImpl) beanManager;
		@SuppressWarnings("unchecked")
		AbstractBoundContext<S> context = (AbstractBoundContext<S>) beanManagerImpl.getContexts().get(cls).get(0);

		context.associate(storage);
		context.activate();
	}

}
